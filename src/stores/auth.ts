import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
  const dialogVisible = ref(true)
  const users = ref<User[]>([
    {
      id: 1,
      email: 'ma1@gmail.com',
      password: 'pass',
      fullName: 'Sivat Rattanavong',
      gender: 'Male',
      roles: ['Manager'],
      logintime: '10:00:00',
      logouttime: '16:00:00',
      date: new Date()
    },

    {
      id: 2,
      email: 'ma2@gmail.com',
      password: 'pass',
      fullName: 'Tanakit Wongviroj',
      gender: 'Male',
      roles: ['Employee'],
      logintime: '10:00:00',
      logouttime: '16:00:00',
      date: new Date()
    },
    {
      id: 3,
      email: 'ma3@gmail.com',
      password: 'pass',
      fullName: 'Kornnisa Tongaeim',
      gender: 'Female',
      roles: ['Manager'],
      logintime: '10:00:00',
      logouttime: '16:00:00',
      date: new Date()
    },
    {
      id: 4,
      email: 'ma4@gmail.com',
      password: 'pass',
      fullName: 'Jane Jajo',
      gender: 'Female',
      roles: ['Employee'],
      logintime: '10:00:00',
      logouttime: '16:00:00',
      date: new Date()
    },
    {
      id: 5,
      email: 'ma5@gmail.com',
      password: 'pass',
      fullName: 'Phu Fah',
      gender: 'Female',
      roles: ['Employee'],
      logintime: '10:00:00',
      logouttime: '16:00:00',
      date: new Date()
    },
    {
      id: 6,
      email: 'ma6@gmail.com',
      password: 'pass',
      fullName: 'Pare Lolipop',
      gender: 'Female',
      roles: ['Employee'],
      logintime: '10:00:00',
      logouttime: '16:00:00',
      date: new Date()
    },
    {
      id: 7,
      email: 'ma7@gmail.com',
      password: 'pass',
      fullName: 'Arm Koi',
      gender: 'Male',
      roles: ['Employee'],
      logintime: '10:00:00',
      logouttime: '16:00:00',
      date: new Date()
    }
  ])
  const erDialog = ref(false)
  const currentUser = ref<User | null>()
  const checkUser = (email: string, password: string) => {
    console.log('Checking user with email:', email, 'and password:', password)
    const user = users.value.find(
      (item) => item.email.trim() === email.trim() && item.password === password.trim()
    )
    if (user) {
      updateCurrentUser(user)
      dialogVisible.value = false
    } else {
      updateCurrentUser(null)
      dialogVisible.value = true
      window.alert("Please Login Again.")
    }
  }
  const Logout = () => {
    dialogVisible.value = true
    updateCurrentUser(null)
  }
  const updateCurrentUser = (user: User | null) => {
      currentUser.value = user
  }
  return { currentUser, checkUser, updateCurrentUser, users, dialogVisible, erDialog, Logout }
})
