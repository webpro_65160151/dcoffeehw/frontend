import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import branchService from '@/services/branch'
import type { Branch } from '@/types/Branch'

export const useBranchStore = defineStore('branch', () => {
  const loadingStore = useLoadingStore()
  const branchs = ref<Branch[]>([])

  async function getBranch(id: number) {
    loadingStore.doLoad()
    const res = await branchService.getBranch(id)
    branchs.value = res.data
    loadingStore.finish()
  }
  //Data function

  async function getBranchs() {
    loadingStore.doLoad()
    const res = await branchService.getBranchs()
    branchs.value = res.data
    loadingStore.finish()
  }
  async function saveBranch(branch: Branch) {
    loadingStore.doLoad()
    if (branch.id < 0) {
      //Add new
      console.log('Post' + JSON.stringify(branch))
      const res = await branchService.addBranch(branch)
    } else {
      // Update
      console.log('Patch' + JSON.stringify(branch))
      const res = await branchService.updateBranch(branch)
    }
    await getBranchs()
    loadingStore.finish()
  }

  async function deleteBranch(branch: Branch) {
    loadingStore.doLoad()
    const res = await branchService.delBranch(branch)
    await getBranchs()
    loadingStore.finish()
  }
  return { branchs, getBranchs, saveBranch, deleteBranch, getBranch }
})
