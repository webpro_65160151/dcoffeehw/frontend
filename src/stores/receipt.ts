import { ref, type Ref } from 'vue'
import { defineStore } from 'pinia'
import type { Product } from '@/types/Product'
import type { ReceiptItem } from '@/types/RecieptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'

export const useReceiptStore = defineStore('receipt', () => {
  const memberStore = useMemberStore()
  const authStore = useAuthStore()
  const receiptDialog = ref(false)
  const qrDialog = ref(false)
  const erDialog = ref(false)
  const errorCashDialog = ref(false)
  const receipt = ref<Receipt>({
    id: 0,
    createdDate: new Date(),
    total: 0,
    totalNet: 0,
    memberDiscount: 0,
    receivedAmount: 0,
    change: 0,
    paymentType: '',
    userId: authStore.users[0].id,
    user: authStore.users[0],
    memberId: 0
  })
  const receiptItems = ref<ReceiptItem[]>([])

  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)
    if (index >= 0) {
      receiptItems.value[index].unit++
      calReceipt()
      return
    } else {
      const newReceipt: ReceiptItem = {
        id: -1,
        name: product.name,
        price: product.price,
        sweetLevel: 50,
        unit: 1,
        productId: product.id,
        product: product
      }
      receiptItems.value.push(newReceipt)
      calReceipt()
    }
  }

  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }

  function incSL(item: ReceiptItem) {
    const increment = 25
    if (item.sweetLevel + increment > 126) {
      window.alert("Can't increase the sweetness value beyond 125.")
    } else {
      item.sweetLevel += increment
    }
  }

  function decSL(item: ReceiptItem) {
    const decrement = 25
    if (item.sweetLevel - decrement < -1) {
      window.alert("Can't decrease the sweetness value beyond 125.")
    } else {
      item.sweetLevel -= decrement
    }
  }

  function inc(item: ReceiptItem) {
    item.unit++
    calReceipt()
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    }
    item.unit--
    calReceipt()
  }
  function calReceipt() {
    let total = 0
    for (const item of receiptItems.value) {
      total = total + item.price * item.unit
    }
    const cash = receipt.value.receivedAmount
    receipt.value.total = total
    receipt.value.memberDiscount = 0
    if (memberStore.currentMember) {
      receipt.value.memberDiscount = total - total * 0.95
      receipt.value.totalNet = total * 0.95
      receipt.value.change = cash - receipt.value.totalNet
    } else {
      receipt.value.totalNet = total
      receipt.value.change = cash - receipt.value.totalNet
    }
  }

  function showReceiptDialog() {
    if (receipt.value.change < 0) {
      errorCashDialog.value = true
    } else if (receipt.value.receivedAmount === 0) {
      errorCashDialog.value = true
    } else if (receipt.value.paymentType === '') {
      erDialog.value = true
    } else {
      if (receipt.value.paymentType === 'Qr-Code') {
        qrDialog.value = true
      } else {
        if (
          receipt.value.paymentType !== '' &&
          receipt.value.receivedAmount !== 0 &&
          receipt.value.change! < 0
        ) {
          receiptDialog.value = true
          receipt.value.receiptItems = receiptItems.value
        }
      }
    }
  }
  function clear() {
    receiptItems.value = []
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      total: 0,
      totalNet: 0,
      memberDiscount: 0,
      receivedAmount: 0,
      change: 0,
      paymentType: '',
      userId: authStore.users[0].id,
      user: authStore.users[0],
      memberId: 0
    }
    memberStore.clear()
  }

  return {
    addReceiptItem,
    removeReceiptItem,
    inc,
    dec,
    receiptItems,
    receipt,
    calReceipt,
    receiptDialog,
    showReceiptDialog,
    clear,
    incSL,
    decSL,
    qrDialog,
    errorCashDialog,
    erDialog
  }
})
