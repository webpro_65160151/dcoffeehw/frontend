type Branch = {
  id: number
  name: string
  email: string
  address: string
  tel: string
}

export type { Branch }
