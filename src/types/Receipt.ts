import type { Member } from './Member'
import type { ReceiptItem } from './RecieptItem'
import type { User } from './User'

type Receipt = {
  id: number
  createdDate: Date
  total: number
  totalNet: number
  memberDiscount: number
  receivedAmount: number
  change: number
  paymentType: string
  userId: number
  user?: User
  memberId: number
  member?: Member
  receiptItems?: ReceiptItem[]
}

export type { Receipt }
